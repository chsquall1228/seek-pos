package com.seek.pos.domains;

public interface IHandler<TRequest, TResponse> {
    TResponse handle(TRequest request);
}

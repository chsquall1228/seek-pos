package com.seek.pos.domains;

public class GenerateTotalPrice {
    private String customer;
    private String[] ads;

    public GenerateTotalPrice(String customer, String[] ads){
        this.customer = customer;
        this.ads = ads;
    }

    public String getCustomer() {
        return customer;
    }

    public void setCustomer(String customer) {
        this.customer = customer;
    }

    public String[] getAds() {
        return ads;
    }

    public void setAds(String[] ads) {
        this.ads = ads;
    }
}

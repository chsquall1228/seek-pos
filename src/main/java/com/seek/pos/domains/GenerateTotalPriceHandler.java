package com.seek.pos.domains;

import com.seek.pos.exceptions.InvalidArgumentException;
import com.seek.pos.exceptions.NotFoundException;
import com.seek.pos.models.Ad;
import com.seek.pos.models.Customer;
import com.seek.pos.repositories.IAdRepository;
import com.seek.pos.repositories.ICustomerRepository;
import com.seek.pos.services.Checkout;
import com.seek.pos.services.ICheckout;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayList;
import java.util.List;

@Service("generateTotalPrice")
@Scope("prototype")
public class GenerateTotalPriceHandler implements IHandler<GenerateTotalPrice, Double>{
    private final ICustomerRepository customerRepository;
    private final IAdRepository adRepository;

    private List<Ad> ads = new ArrayList<>();
    private Customer customer;

    public GenerateTotalPriceHandler(ICustomerRepository customerRepository, IAdRepository adRepository){
        this.customerRepository = customerRepository;
        this.adRepository = adRepository;
    }

    @Override
    public Double handle(GenerateTotalPrice request) {
        this.validate(request);
        Customer customer = this.customerRepository.get(request.getCustomer());
        ICheckout checkout = new Checkout(customer.getPriceRules());
        this.ads.forEach(checkout::add);
        return checkout.total();
    }

    public void validate(GenerateTotalPrice request){
        if(StringUtils.isEmpty(request.getCustomer())){
            throw new InvalidArgumentException("Customer input is empty");
        }

        if(request.getAds() == null){
            throw new InvalidArgumentException("SKU is empty");
        }

        for(String item: request.getAds()){
            try{
                this.ads.add(this.adRepository.get(item.trim()));
            }catch(NotFoundException ex){
                throw new InvalidArgumentException("SKU invalid");
            }
        }

    }
}

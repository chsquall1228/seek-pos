package com.seek.pos.models;

import com.seek.pos.services.rules.IRule;

import java.util.ArrayList;
import java.util.List;

public class Customer {

    private final String id;
    private final List<IRule> priceRules = new ArrayList<>();

    public Customer(){
        this("default");
    }

    public Customer(String id) {
        this.id = id;
    }

    public void addRule(IRule rule){
        if(this.priceRules.stream().anyMatch(item -> rule.getAd().equals(item.getAd()))){
            throw new IllegalArgumentException("Similar advertisement rule added before");
        }
        this.priceRules.add(rule);
    }

    public String getId() {
        return id;
    }

    public List<IRule> getPriceRules() {
        return priceRules;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Customer){
            return ((Customer) obj).id.equals(this.id);
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }
}

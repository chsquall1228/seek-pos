package com.seek.pos.models;

public class Ad {
    private final String id;
    private final String name;
    private final double price;
    private final boolean isLogoAllow;
    private final boolean isLongerPresentationAllow;
    private final int priority;

    public Ad(String id, String name, double price){
        this(id, name, price, false, false, 1);
    }


    public Ad(String id, String name, double price, boolean isLogoAllow, boolean isLongerPresentationAllow){
        this(id, name, price, isLogoAllow, isLongerPresentationAllow, 1);
    }


    public Ad(String id, String name, double price, boolean isLogoAllow, boolean isLongerPresentationAllow, int priority){
        this.id = id;
        this.name = name;
        this.price = price;
        this.isLogoAllow = isLogoAllow;
        this.isLongerPresentationAllow = isLongerPresentationAllow;
        this.priority = priority;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public double getPrice() {
        return price;
    }

    public boolean isLogoAllow(){
        return this.isLogoAllow;
    }

    public boolean isLongerPresentationAllow(){
        return this.isLongerPresentationAllow;
    }

    public int getPriority(){
        return this.priority;
    }

    @Override
    public boolean equals(Object obj) {
        if(obj instanceof Ad){
            return ((Ad) obj).getId().equals(this.id);
        }
        return super.equals(obj);
    }

    @Override
    public int hashCode() {
        return this.id.hashCode();
    }
}

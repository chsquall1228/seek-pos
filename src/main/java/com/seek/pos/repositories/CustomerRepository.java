package com.seek.pos.repositories;

import com.seek.pos.constants.SKU;
import com.seek.pos.models.Customer;
import com.seek.pos.services.rules.DiscountRule;
import com.seek.pos.services.rules.ForRule;
import org.springframework.stereotype.Repository;

import java.util.*;

@Repository
public class CustomerRepository implements ICustomerRepository {
    private final List<Customer> customers = new ArrayList<>();
    private final Customer defaultCustomer;

    public CustomerRepository(IAdRepository adRepository){
        this.defaultCustomer = new Customer();

        Customer unilever = new Customer("Unilever");
        unilever.addRule(new ForRule(3, 2, adRepository.get(SKU.CLASSIC)));

        Customer apple = new Customer("Apple");
        apple.addRule(new DiscountRule(1, 299.99, adRepository.get(SKU.STANDOUT)));

        Customer nike = new Customer("Nike");
        nike.addRule(new DiscountRule(4, 379.99, adRepository.get(SKU.PREMIUM)));

        Customer ford = new Customer("Ford");
        ford.addRule(new ForRule(5, 4, adRepository.get(SKU.CLASSIC)));
        ford.addRule(new DiscountRule(1, 309.99, adRepository.get(SKU.STANDOUT)));
        ford.addRule(new DiscountRule(3, 389.99, adRepository.get(SKU.PREMIUM)));

        this.customers.add(unilever);
        this.customers.add(apple);
        this.customers.add(nike);
        this.customers.add(ford);
    }

    @Override
    public Customer get(String id) {
        Optional<Customer> customerOptional = this.customers.stream().filter(customer -> customer.getId().equals(id)).findFirst();
        return customerOptional.orElse(defaultCustomer);
    }
}

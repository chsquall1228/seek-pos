package com.seek.pos.repositories;

import com.seek.pos.models.Customer;

public interface ICustomerRepository {
    Customer get(String id);
}

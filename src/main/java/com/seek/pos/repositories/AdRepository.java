package com.seek.pos.repositories;

import com.seek.pos.exceptions.NotFoundException;
import com.seek.pos.models.Ad;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;
import java.util.List;

@Repository
public class AdRepository implements IAdRepository{
    private final List<Ad> ads = new ArrayList<>();
    public AdRepository(){
        this.ads.add(new Ad("classic", "Classic Ad", 269.99));
        this.ads.add(new Ad("standout", "Standout Ad", 322.99, true, true));
        this.ads.add(new Ad("premium", "Premium Ad", 394.99, true, true, 10));
    }

    @Override
    public Ad get(String id) {
        return this.ads.stream().filter(ad -> ad.getId().equals(id)).findFirst().orElseThrow(()-> new NotFoundException(id));
    }
}

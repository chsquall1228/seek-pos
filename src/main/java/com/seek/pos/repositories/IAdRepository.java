package com.seek.pos.repositories;

import com.seek.pos.models.Ad;

public interface IAdRepository {
    Ad get(String id);
}

package com.seek.pos;

import com.seek.pos.domains.GenerateTotalPrice;
import com.seek.pos.domains.IHandler;
import com.seek.pos.exceptions.InvalidArgumentException;
import com.seek.pos.services.IConsoleService;
import com.seek.pos.utils.FormatHelper;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.ApplicationContext;

import java.util.Scanner;

@SpringBootApplication
public class PosApplication implements CommandLineRunner {
	private final ApplicationContext applicationContext;
	private final IConsoleService consoleRepository;
	public PosApplication(ApplicationContext applicationContext, IConsoleService consoleRepository){
		this.applicationContext = applicationContext;
		this.consoleRepository = consoleRepository;
	}

	public static void main(String[] args) {
		SpringApplication.run(PosApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		Scanner scanner = new Scanner(System.in);
		boolean exit;
		do{
			try
			{
				consoleRepository.info("Customer: ", false);
				String customerId = scanner.nextLine();
				consoleRepository.info("SKUs Scanned: ", false);
				String ads = scanner.nextLine();
				IHandler<GenerateTotalPrice, Double> handler = (IHandler<GenerateTotalPrice, Double>) applicationContext.getBean("generateTotalPrice");
				Double total = handler.handle(new GenerateTotalPrice(customerId, ads.split("\\s*,\\s*")));
				consoleRepository.info("Total expected: " + FormatHelper.moneyFormat.format(total), false);
			}catch (InvalidArgumentException ex ){
				consoleRepository.warn(ex.getMessage(), true);
			}
			consoleRepository.info("", true);
			consoleRepository.info("Enter exit to close / Enter to continue", true);
			String action = scanner.nextLine();
			exit = action.equalsIgnoreCase("exit");
		}while(!exit);
		scanner.close();
		System.exit(0);
	}
}

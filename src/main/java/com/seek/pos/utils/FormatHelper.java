package com.seek.pos.utils;

import java.text.DecimalFormat;

public class FormatHelper {
    public static final DecimalFormat moneyFormat = new DecimalFormat("$ #.##");
}

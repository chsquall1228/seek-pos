package com.seek.pos.services;

import com.seek.pos.models.Ad;

public interface ICheckout {
    void add(Ad ad);
    Double total();
}

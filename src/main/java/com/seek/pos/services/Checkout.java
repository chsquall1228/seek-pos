package com.seek.pos.services;

import com.seek.pos.models.Ad;
import com.seek.pos.services.rules.IRule;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static java.util.stream.Collectors.groupingBy;

public class Checkout implements ICheckout{
    private final List<IRule> pricingRules;
    private final List<Ad> items = new ArrayList<>();

    public Checkout(List<IRule> pricingRules) {
        this.pricingRules = pricingRules;
    }

    public void add(Ad ad) {
        items.add(ad);
    }

    @Override
    public Double total() {
        double total = 0.0;
        Map<Ad, List<Ad>> groupItems = this.items.stream().collect(groupingBy(item -> item));
        for (Map.Entry<Ad, List<Ad>> entry : groupItems.entrySet()) {
            Ad item = entry.getKey();
            List<Ad> value = entry.getValue();
            boolean hasRule = false;
            for (IRule rule : pricingRules) {
                if ((hasRule = rule.isAllow(item))) {
                    total += rule.getTotalPrice(value.size());
                    break;
                }
            }
            if (!hasRule) {
                total += item.getPrice() * value.size();
            }
        }
        return total;
    }
}

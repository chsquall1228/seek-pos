package com.seek.pos.services.rules;

import com.seek.pos.models.Ad;

public abstract class BaseRule {
    protected final Ad ad;

    protected BaseRule(Ad ad) {
        this.ad = ad;
    }

    public Ad getAd(){
        return this.ad;
    }

    public boolean isAllow(Ad ad){
        return this.ad.getId().equals(ad.getId());
    }

}

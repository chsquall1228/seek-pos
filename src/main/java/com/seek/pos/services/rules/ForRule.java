package com.seek.pos.services.rules;

import com.seek.pos.models.Ad;

public class ForRule extends BaseRule implements IRule{
    private final int minItem;
    private final int totalChargeItems;

    public ForRule(int minItem, int totalChargeItems, Ad ad) {
        super(ad);
        this.minItem = minItem;
        this.totalChargeItems = totalChargeItems;
    }


    @Override
    public double getTotalPrice(int totalItems) {
        if(totalItems  == 0){
            return 0.0;
        }
        int totalDiscountAttired = totalItems / this.minItem;
        int totalRemain = totalItems % this.minItem;
        int totalChargeableItems  = ( totalDiscountAttired * this.totalChargeItems ) + totalRemain;
        return totalChargeableItems * this.ad.getPrice();
    }

}

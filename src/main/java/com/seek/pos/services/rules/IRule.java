package com.seek.pos.services.rules;


import com.seek.pos.models.Ad;

public interface IRule {
    boolean isAllow(Ad item);
    double getTotalPrice(int totalItems);
    Ad getAd();
}

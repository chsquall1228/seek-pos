package com.seek.pos.services.rules;

import com.seek.pos.models.Ad;

public class DiscountRule extends BaseRule implements IRule{
    private final int minItems;
    private final double discountedPrice;

    public DiscountRule(int minItems, double discountedPrice,  Ad ad) {
        super(ad);
        this.minItems = minItems;
        this.discountedPrice = discountedPrice;
    }

    public double getTotalPrice(int totalItems) {
        if(totalItems == 0){
            return 0.0;
        }
        if(totalItems >= this.minItems){
            return totalItems * this.discountedPrice;
        }
        return totalItems * ad.getPrice();
    }
}

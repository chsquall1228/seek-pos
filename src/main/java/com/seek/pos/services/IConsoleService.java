package com.seek.pos.services;

public interface IConsoleService {
    void warn(String msg, boolean newLine);
    void info(String msg, boolean newLine);
}

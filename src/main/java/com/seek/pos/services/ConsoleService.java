package com.seek.pos.services;

import org.springframework.stereotype.Component;

@Component
public class ConsoleService implements IConsoleService {

    public void warn(String msg, boolean newLine){
        this.print(msg, newLine);
    }

    public void info(String msg, boolean newLine){
        this.print(msg, newLine);
    }

    private void print (String msg, boolean newLine){
        if(newLine){
            System.out.println(msg);
        }else{
            System.out.print(msg);
        }
    }
}

package com.seek.pos.constants;

public class SKU {
    public static final String CLASSIC = "classic";
    public static final String STANDOUT = "standout";
    public static final String PREMIUM = "premium";
}

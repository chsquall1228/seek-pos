package com.seek.pos.domains;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class GenerateTotalPriceTests {
    @Test
    public void whenInitial_thenAssertSucceed(){
        GenerateTotalPrice request = new GenerateTotalPrice("test", new String[]{});
        Assert.assertEquals("test", request.getCustomer());
        Assert.assertEquals(0, request.getAds().length);
    }
}

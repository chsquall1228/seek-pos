package com.seek.pos.domains;

import com.seek.pos.exceptions.InvalidArgumentException;
import com.seek.pos.exceptions.NotFoundException;
import com.seek.pos.models.Ad;
import com.seek.pos.models.Customer;
import com.seek.pos.repositories.IAdRepository;
import com.seek.pos.repositories.ICustomerRepository;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class GenerateTotalPriceHandlerTests {
    private ICustomerRepository customerRepository;
    private IAdRepository adRepository;

    @Before
    public void before(){
        this.customerRepository = Mockito.mock(ICustomerRepository.class);
        this.adRepository = Mockito.mock(IAdRepository.class);
        Customer customer = Mockito.mock(Customer.class);
        Ad ad = Mockito.mock(Ad.class);
        Mockito.when(ad.getPrice()).thenReturn(2.0);
        Mockito.when(customer.getPriceRules()).thenReturn(new ArrayList<>());
        Mockito.when(this.customerRepository.get(Mockito.any())).thenReturn(customer);
        Mockito.when(this.adRepository.get(Mockito.eq("fake"))).thenThrow(NotFoundException.class);
        Mockito.when(this.adRepository.get(Mockito.eq("test"))).thenReturn(ad);
    }

    @Test(expected = InvalidArgumentException.class)
    public void whenValidateCustomer_thenExpectException(){
        GenerateTotalPrice request = new GenerateTotalPrice(null, null);
        IHandler handler =  new GenerateTotalPriceHandler(this.customerRepository, this.adRepository);
        handler.handle(request);
    }

    @Test(expected = InvalidArgumentException.class)
    public void whenValidateAds_thenExpectException(){
        GenerateTotalPrice request = new GenerateTotalPrice("test", null);
        IHandler handler =  new GenerateTotalPriceHandler(this.customerRepository, this.adRepository);
        handler.handle(request);
    }


    @Test(expected = InvalidArgumentException.class)
    public void whenValidateInvalidAds_thenExpectException(){
        GenerateTotalPrice request = new GenerateTotalPrice("test", new String[]{"fake"});
        IHandler handler =  new GenerateTotalPriceHandler(this.customerRepository, this.adRepository);
        handler.handle(request);
    }

    @Test
    public void whenHandle_thenAssertSucceed(){
        GenerateTotalPrice request = new GenerateTotalPrice("test", new String[]{"test", "test"});
        IHandler<GenerateTotalPrice, Double> handler =  new GenerateTotalPriceHandler(this.customerRepository, this.adRepository);
        Double total = handler.handle(request);
        Assert.assertEquals(4, total, 0);
    }
}

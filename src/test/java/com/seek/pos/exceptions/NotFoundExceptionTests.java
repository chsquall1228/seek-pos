package com.seek.pos.exceptions;

import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class NotFoundExceptionTests {
    @Test
    public void whenInitialException_thenAssertSucceed(){
        Exception exception = new NotFoundException("test");
        Assert.assertEquals("test not found.", exception.getMessage());
    }
}

package com.seek.pos.models;

import com.seek.pos.constants.SKU;
import com.seek.pos.services.rules.DiscountRule;
import com.seek.pos.services.rules.IRule;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CustomerTests {

    @Test
    public void initialDefaultCustomer(){
        Customer customer = new Customer();
        Assert.assertNotNull(customer);
        Assert.assertEquals("default", customer.getId());
        Assert.assertEquals(0, customer.getPriceRules().size());
    }

    @Test
    public void wheninitialCustomer_thenAssertsSucceed(){
        Customer customer = new Customer("Apple");
        Assert.assertEquals("Apple", customer.getId());
    }

    @Test
    public void whenAddRules_thenAssertsSucceed(){
        Ad ad =  new Ad(SKU.PREMIUM, "Premium", 1234);
        IRule rule = new DiscountRule(1, 3.0, ad);
        Customer customer = new Customer("Apple");
        customer.addRule( rule );
        Assert.assertEquals(1, customer.getPriceRules().size());
        Assert.assertEquals(rule, customer.getPriceRules().get(0));
    }

    @Test(expected = IllegalArgumentException.class)
    public void whenaddDuplicateRules_thenExpectException(){
        Ad ad =  new Ad(SKU.PREMIUM, "Premium", 1234);
        IRule rule = new DiscountRule(1, 3.0, ad);
        Customer customer = new Customer("Apple");
        customer.addRule(rule);
        customer.addRule(rule);
    }
}

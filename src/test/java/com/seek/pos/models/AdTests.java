package com.seek.pos.models;

import com.seek.pos.constants.SKU;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class AdTests {
    @Test
    public void whenInitialClassicAd_thenAssertSucceed(){
        Ad ad = new Ad(SKU.CLASSIC, "Classic Ad",  123.0);
        Assert.assertEquals(SKU.CLASSIC, ad.getId());
        Assert.assertEquals("Classic Ad", ad.getName());
        Assert.assertEquals(123.0, ad.getPrice(), 0.0);
        Assert.assertFalse(ad.isLogoAllow());
        Assert.assertFalse(ad.isLongerPresentationAllow());
        Assert.assertEquals(1, ad.getPriority());
    }

    @Test
    public void whenInitialStandoutAd_thenAssertSucceed(){
        Ad ad = new Ad(SKU.STANDOUT, "Standout Ad",  123.0, true, true);
        Assert.assertEquals(SKU.STANDOUT, ad.getId());
        Assert.assertEquals("Standout Ad", ad.getName());
        Assert.assertEquals(123.0, ad.getPrice(), 0.0);
        Assert.assertTrue(ad.isLogoAllow());
        Assert.assertTrue(ad.isLongerPresentationAllow());
        Assert.assertEquals(1, ad.getPriority());
    }

    @Test
    public void whenInitialPremiumAd_thenAssertSucceed(){
        Ad ad = new Ad(SKU.PREMIUM, "Premium Ad",  123.0, true, true, 10);
        Assert.assertEquals(SKU.PREMIUM, ad.getId());
        Assert.assertEquals("Premium Ad", ad.getName());
        Assert.assertEquals(123.0, ad.getPrice(), 0.0);
        Assert.assertTrue(ad.isLogoAllow());
        Assert.assertTrue(ad.isLongerPresentationAllow());
        Assert.assertEquals(10, ad.getPriority());
    }

    @Test
    public void whenEquals_thenAssertSucceed(){
        Ad ad = new Ad(SKU.CLASSIC, "Classic Ad",  123.0);
        Ad ad2 = new Ad(SKU.CLASSIC, "Classic Ad",  3.0);
        Assert.assertEquals(ad, ad2);
    }
}

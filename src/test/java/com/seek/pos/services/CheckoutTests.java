package com.seek.pos.services;

import com.seek.pos.constants.SKU;
import com.seek.pos.models.Ad;
import com.seek.pos.services.rules.IRule;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.ArrayList;
import java.util.Collections;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CheckoutTests {
    private Ad classic;
    private Ad premium;
    private IRule rule;
    @Before
    public void before(){
        classic = new Ad(SKU.CLASSIC, "Classic", 4.0);
        premium = new Ad(SKU.PREMIUM, "Premium", 6.0);
        rule = Mockito.mock(IRule.class);
        Mockito.when(rule.isAllow(Mockito.any())).thenReturn(false);
    }

    @Test
    public void whenGetTotalWithoutRules_thenAssertSucceed(){
        ICheckout checkout = new Checkout(new ArrayList<>());
        checkout.add(classic);
        checkout.add(premium);
        Assert.assertEquals(10, checkout.total(), 0);
    }

    @Test
    public void whenGetTotalWithRules_thenAssertSucced(){
        ICheckout checkout = new Checkout(Collections.singletonList(rule));
        checkout.add(classic);
        checkout.add(premium);
        Assert.assertEquals(10, checkout.total(), 0);
    }
}

package com.seek.pos.services.rules;

import com.seek.pos.constants.SKU;
import com.seek.pos.models.Ad;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class ForRuleTests {
    private Ad classic;
    private Ad premium;
    @Before
    public void before(){
        classic = new Ad(SKU.CLASSIC, "Classic", 4);
        premium = new Ad(SKU.PREMIUM, "Premium",  6);
    }

    @Test
    public void whenInitialRule_whenAssertSucceed(){
        IRule rule = new ForRule(3, 2, classic);
        Assert.assertEquals(classic, rule.getAd());
    }

    @Test
    public void whenTryAddItemPremium_thenAssertSucceed(){
        IRule rule = new ForRule(3, 2, classic);
        boolean success = rule.isAllow(premium);
        Assert.assertFalse(success);
    }


    @Test
    public void whenTryAddItemClassic_thenAssertSucceed(){
        IRule rule = new ForRule(3, 2, classic);
        boolean success = rule.isAllow(classic);
        Assert.assertTrue(success);
    }

    @Test
    public void whenGetTotal_thenAssertSucced(){
        IRule rule = new ForRule(3, 2, classic);
        Assert.assertEquals(12, rule.getTotalPrice(4), 0);
    }
}

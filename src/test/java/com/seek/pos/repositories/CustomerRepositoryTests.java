package com.seek.pos.repositories;

import com.seek.pos.models.Ad;
import com.seek.pos.models.Customer;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class CustomerRepositoryTests {
    private ICustomerRepository customerRepository;
    private IAdRepository adRepository;

    @Before
    public void before(){
        this.adRepository = new AdRepository();
        this.customerRepository = new CustomerRepository(this.adRepository);
    }

    @Test
    public void whenGetCustomer_thenAssertSucceed(){
        Customer customer = this.customerRepository.get("Apple");
        Assert.assertNotNull(customer);
        Assert.assertEquals(customer.getId(), "Apple");
    }

    @Test
    public void whenGetDefaultCustomer_thenAssertSucceed(){
        Customer customer = this.customerRepository.get("tests");
        Assert.assertNotNull(customer);
        Assert.assertEquals(customer.getId(), "default");
    }
}

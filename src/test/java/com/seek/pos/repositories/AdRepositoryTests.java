package com.seek.pos.repositories;

import com.seek.pos.constants.SKU;
import com.seek.pos.exceptions.NotFoundException;
import com.seek.pos.models.Ad;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.util.Assert;

@RunWith(MockitoJUnitRunner.class)
@SpringBootTest
public class AdRepositoryTests {
    private IAdRepository repository;

    @Before
    public void before(){
        this.repository = new AdRepository();
    }

    @Test
    public void whenGetClassic_thenAssertSucceed(){
        Ad ad = this.repository.get(SKU.CLASSIC);
        Assert.notNull(ad, "classic ad not found");
    }

    @Test
    public void whenGetStandout_thenAssertSucceed(){
        Ad ad = this.repository.get(SKU.STANDOUT);
        Assert.notNull(ad, "standout ad not found");
    }

    @Test
    public void whenGetPremium_thenAssertSucceed(){
        Ad ad = this.repository.get(SKU.PREMIUM);
        Assert.notNull(ad, "premium ad not found");
    }

    @Test(expected = NotFoundException.class)
    public void whenGetUnknown_thenExpectException(){
        this.repository.get("test");
    }
}

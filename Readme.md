# Coding Assignment
Developer: Lim Chin Hau
Platform Used: Windows
Language Used: JAVA (jdk11)
Framework: spring

#Coding Guide
1. CQRS pattern and Repository
2. 2 repository created for storing customer and advertisement collection allow to replace with sql in further implementation
3. A basic cli to receive customer id and sku to retrieve total price
4. JUnit for unit testing
5. /src/main/java is related code path, /src/test/java is related unit test path

#Execution Guide
1. jdk11 is required for window and  openjdk is required for linux
2. all test report and program is located in /target
3. make sure java is located in environment path to execute
3. "java -jar pos-1.0.0.jar" is a simple scripting to execute the program

Thanks For the Reviewing
Regards Lim Chin Hau